//
//  AppDelegate.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Public properties
    
    let appAssembly = AppAssembly()
    
    // MARK: - Private properties
    
    private lazy var appDelegates = AppDelegatesFactory.makeDefault()
    
    // MARK: - UIApplicationDelegate

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        _ = appDelegates.application?(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
}
