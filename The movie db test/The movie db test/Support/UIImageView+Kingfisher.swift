//
//  UIImageView+Kingfisher.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Kingfisher

extension UIImageView {
    
    func setImageWithUrl(_ url: URL?) {
        setImageWithUrl(url, useActivity: true)
    }
    
    func setImageWithUrl(_ url: URL?, useActivity: Bool) {
        kf.indicatorType = useActivity ? .activity : .none
        kf.setImage(with: url)
    }
    
    func setImageWithUrl(url: URL?,
                         useActivity: Bool,
                         placeholder: UIImage?,
                         completion: Callback<Result<UIImage, Error>>? = nil) {
        kf.indicatorType = useActivity ? .activity : .none
        
        kf.setImage(with: url,
                    placeholder: placeholder,
                    completionHandler: { (result) in
                        switch result {
                        case .success(let image):
                            DispatchQueue.main.async { [weak self] in
                                self?.image = image.image
                            }
                            completion?(.success(image.image))
                        case .failure(let error):
                            completion?(.failure(error))
                        }
                    })
    }
    
    func animateWithFade(image: UIImage, duration: TimeInterval = 1) {
        UIView.transition(with: self,
                          duration: duration,
                          options: .transitionCrossDissolve, animations: { [weak self] in
            self?.image = image
        }, completion: nil)
    }
}
