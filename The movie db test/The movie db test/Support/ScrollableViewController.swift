//
//  ScrollableViewController.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

protocol ScrollableViewController: class {
    var scrollView: UIScrollView! { get }
    func scrollViewContentInsetFor(keyboardEndFrame: CGRect, safeAreaInsets: UIEdgeInsets) -> UIEdgeInsets
    func addKeyboardObservers()
    func removeKeyboardObservers()
}

extension ScrollableViewController where Self: UIViewController {

    func scrollViewContentInsetFor(keyboardEndFrame: CGRect, safeAreaInsets: UIEdgeInsets) -> UIEdgeInsets {
        let keyboardOffset: CGFloat = safeAreaInsets.bottom == 0.0 ? 20.0 : 10.0
        let contentInset = UIEdgeInsets(
            top: 0,
            left: 0,
            bottom: keyboardEndFrame.height - safeAreaInsets.bottom + keyboardOffset,
            right: 0
        )
        
        return contentInset
    }
    
    func addKeyboardObservers() {
        var notifications: [Any?] = []
        scrollView.showsVerticalScrollIndicator = false

        let willChangeFrame = NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillChangeFrameNotification,
            object: nil,
            queue: .main
        ) { [weak self] notification in
            guard let self = self else { return }
            guard let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
            let keyboardScreenEndFrame = keyboardSize.cgRectValue
            let keyboardViewEndFrame = self.view.convert(keyboardScreenEndFrame, from: self.view.window)
            let safeAreaBottomInset = self.view.safeAreaInsets
            
            self.scrollView.contentInset = self.scrollViewContentInsetFor(keyboardEndFrame: keyboardViewEndFrame,
                                                                          safeAreaInsets: safeAreaBottomInset)
            
            UIView.animate(withDuration: 0.25) { self.view.layoutIfNeeded() }
        }

        let willHide = NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillHideNotification,
            object: nil,
            queue: .main
        ) { [weak self] _ in
            guard let self = self else { return }
            self.scrollView.contentInset = .zero
            self.scrollView.scrollIndicatorInsets = .zero
            UIView.animate(withDuration: 0.25) { self.view.layoutIfNeeded() }
        }
        notifications = [willChangeFrame, willHide]
        _ = notifications
    }

    func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil
        )
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
}
