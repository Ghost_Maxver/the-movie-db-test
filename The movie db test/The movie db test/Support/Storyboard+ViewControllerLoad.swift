//
//  Storyboard+ViewControllerLoad.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

extension UIStoryboard {
    static func loadViewController<V>(fromStoryboardNamed storyboardName: String = "\(V.self)") -> V {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.loadViewController()
    }
    
    func loadViewController<V>() -> V {
        guard let viewController = instantiateInitialViewController() as? V else {
            fatalError("Couldn't instantiate ViewController of \(V.self) type")
        }
        
        return viewController
    }
}
