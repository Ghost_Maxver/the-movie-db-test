//
//  UIViewController+Alert.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 03.12.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

extension UIViewController {
    @discardableResult
    func showAlertController(title: String?,
                             message: String?,
                             cancelButtonTitle: String? = "OK") -> UIAlertController? {
        
        if title == nil && message == nil {
            return nil
        }
        
        return showAlertController(title: title,
                                   message: message,
                                   cancelButtonTitle: cancelButtonTitle,
                                   otherButtonTitles: nil)
        
    }

    @discardableResult
    func showAlertController(title: String?,
                             message: String?,
                             cancelButtonTitle: String? = "OK",
                             otherButtonTitles: [String]?,
                             handler: Callback<Int>? = nil) -> UIAlertController {
        return UIViewController.showAlertController(title: title,
                                                    message: message,
                                                    fromViewController: self,
                                                    cancelButtonTitle: cancelButtonTitle,
                                                    otherButtonTitles: otherButtonTitles,
                                                    handler: handler)
    }
    
    @discardableResult
    func showSheetController(title: String?,
                             message: String?,
                             cancelButtonTitle: String? = "OK",
                             otherButtonTitles: [String]?,
                             handler: Callback<Int>? = nil) -> UIAlertController {
        return UIViewController.showSheetController(title: title,
                                                    message: message,
                                                    fromViewController: self,
                                                    cancelButtonTitle: cancelButtonTitle,
                                                    otherButtonTitles: otherButtonTitles,
                                                    handler: handler)
    }
    
    @discardableResult
    class func showAlertController(title: String?,
                                   message: String?,
                                   fromViewController viewController: UIViewController,
                                   cancelButtonTitle: String? = "OK",
                                   otherButtonTitles: [String]?,
                                   handler: Callback<Int>? = nil) -> UIAlertController {
        return showAlertController(style: .alert,
                                   title: title,
                                   message: message,
                                   fromViewController: viewController,
                                   cancelButtonTitle: cancelButtonTitle,
                                   otherButtonTitles: otherButtonTitles,
                                   handler: handler)
    }
    
    @discardableResult
    class func showSheetController(title: String?,
                                   message: String?,
                                   fromViewController viewController: UIViewController,
                                   cancelButtonTitle: String? = "OK",
                                   otherButtonTitles: [String]?,
                                   handler: Callback<Int>? = nil) -> UIAlertController {
        return showAlertController(style: .actionSheet,
                                   title: title,
                                   message: message,
                                   fromViewController: viewController,
                                   cancelButtonTitle: cancelButtonTitle,
                                   otherButtonTitles: otherButtonTitles,
                                   handler: handler)
    }
}

private extension UIViewController {
    @discardableResult
    class func showAlertController(style: UIAlertController.Style,
                                   title: String?,
                                   message: String?,
                                   fromViewController viewController: UIViewController,
                                   cancelButtonTitle: String? = "OK",
                                   otherButtonTitles: [String]?,
                                   handler: Callback<Int>? = nil) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle,
                                         style: .cancel) { _ in
            handler?(0)
        }
        
        alertController.addAction(cancelAction)
        
        otherButtonTitles?.enumerated().forEach { tuple in
            let action = UIAlertAction(title: tuple.element,
                                       style: .default) { _ in
                handler?(tuple.offset + 1)
            }

            alertController.addAction(action)
        }
        
        viewController.present(alertController, animated: true, completion: nil)
        
        return alertController
    }
}
