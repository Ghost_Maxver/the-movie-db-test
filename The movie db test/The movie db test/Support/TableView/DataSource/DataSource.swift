//
//  DataSource.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

class DataSource<Item> {
    var sections: [Section<Item>]
    var numberOfSections: Int { sections.count }
    
    func numberOfItems(in section: Int) -> Int {
        guard section < self.sections.count else { return 0 }
        return sections[section].numberOfItems
    }
    
    func item(at indexPath: IndexPath) -> Item {
        return sections[indexPath.section].items[indexPath.row]
    }
    
    func remove(at indexPath: IndexPath) -> Item {
        return sections[indexPath.section].items.remove(at: indexPath.row)
    }
    
    func insert(_ item: Item, at indexPath: IndexPath) {
        sections[indexPath.section].items.insert(item, at: indexPath.row)
    }
    
    var isEmpty: Bool {
        let numberOfItemsInDataSource = self.sections.reduce(0) { (previousResult, section) -> Int in
            return previousResult + section.items.count
        }
        
        return numberOfItemsInDataSource <= 0
    }
    
    init() {
        sections = []
    }
    
    init(sections: [Section<Item>]) {
        self.sections = sections
    }
}

class Section<Item> {
    var numberOfItems: Int { items.count }
    var items: [Item]
    var title: String?
    
    init(items: [Item], title: String? = nil) {
        self.items = items
        self.title = title
    }
}
