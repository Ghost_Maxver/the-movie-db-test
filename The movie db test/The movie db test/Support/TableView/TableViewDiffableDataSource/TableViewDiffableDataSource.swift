//
//  TableViewDiffableDataSource.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

// swiftlint:disable colon opening_brace

class TableViewDiffableDataSource<SectionIdentifier: Hashable, ItemIdentifier: Hashable>:
    UITableViewDiffableDataSource<SectionIdentifier, ItemIdentifier>
{
    /// If `true` - `tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool`
    /// always returns `true` otherwise `false`
    var isRowsEditable: Bool = false
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return isRowsEditable
    }
}

// swiftlint:enable colon opening_brace
