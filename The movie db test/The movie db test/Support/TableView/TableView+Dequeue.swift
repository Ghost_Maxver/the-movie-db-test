//
//  TableView+Dequeue.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

// MARK: - ReuseIdentifiable

protocol ReuseIdentifiable {
    static var reuseIdentifier: String { get }
}

extension ReuseIdentifiable {
    static var reuseIdentifier: String {
        return "\(self)"
    }
}

// MARK: - NibProvidable

protocol NibProvidable: class {
    static var nibName: String { get }
    static var nib: UINib { get }
}

extension NibProvidable {
    static var nibName: String { "\(self)" }
    static var nib: UINib {
        return UINib(nibName: nibName, bundle: Bundle(for: self))
    }
}

// MARK: - UITableView

extension UITableView {
    
    // MARK: Cell -
    
    func registerClass<C>(_ cellClass: C.Type) where C: UITableViewCell & ReuseIdentifiable {
        register(cellClass, forCellReuseIdentifier: C.reuseIdentifier)
    }
    
    func registerNib<C>(_ cellClass: C.Type) where C: UITableViewCell & ReuseIdentifiable & NibProvidable {
        self.register(C.nib, forCellReuseIdentifier: C.reuseIdentifier)
    }
    
    func dequeueCell<C>(at indexPath: IndexPath) -> C where C: UITableViewCell & ReuseIdentifiable {
        return self.dequeueCell(at: indexPath, reuseIdentifier: C.reuseIdentifier)
    }
    
    func dequeueCell<C: UITableViewCell>(at indexPath: IndexPath, reuseIdentifier: String) -> C {
        guard let cell = dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? C else {
            fatalError("Couldn't dequeue \(C.self) cell from \(self) UITableView")
        }
        
        return cell
    }
    
    // MARK: HeaderFooter -
    
    func registerHeaderFooterClass<T>(_ headerFooterClass: T.Type) where T: UITableViewHeaderFooterView & ReuseIdentifiable {
        register(headerFooterClass, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableHeaderFooterView<T>() -> T where T: UITableViewHeaderFooterView & ReuseIdentifiable {
        guard let headerFooterView = self.dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T else {
            fatalError("Couldn't dequeue \(T.self) headerFooterView from \(self) UITableView")
        }
        
        return headerFooterView
    }
}
