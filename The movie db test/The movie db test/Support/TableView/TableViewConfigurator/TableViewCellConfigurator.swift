//
//  TableViewCellConfigurator.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

protocol TableViewCellConfigurable {
    associatedtype Item
    associatedtype Cell: UITableViewCell
    
    func reuseIdentifier(for item: Item, at indexPath: IndexPath) -> String
    func configuredCell(for item: Item, tableView: UITableView, indexPath: IndexPath) -> Cell
    func registerCells(in tableView: UITableView)
}

struct TableViewCellConfigurator<Cell: UITableViewCell, Item>: TableViewCellConfigurable {
    typealias CellConfigurator = (Cell, Item, UITableView, IndexPath) -> Cell
    
    let configurator: CellConfigurator
    let reuseIdentifier = "\(Cell.self)"
    
    func reuseIdentifier(for item: Item, at indexPath: IndexPath) -> String {
        return reuseIdentifier
    }
    
    func configureCell(_ cell: Cell, item: Item, tableView: UITableView, indexPath: IndexPath) -> Cell {
        return configurator(cell, item, tableView, indexPath)
    }
    
    func configuredCell(for item: Item, tableView: UITableView, indexPath: IndexPath) -> Cell {
        let reuseIdentifier = self.reuseIdentifier(for: item, at: indexPath)
        let cell: Cell = tableView.dequeueCell(at: indexPath, reuseIdentifier: reuseIdentifier)
        return self.configureCell(cell, item: item, tableView: tableView, indexPath: indexPath)
    }
    
    func registerCells(in tableView: UITableView) {
        
        if let path = Bundle.main.path(forResource: "\(Cell.self)", ofType: "nib"),
            FileManager.default.fileExists(atPath: path) {
            tableView.register(UINib(nibName: "\(Cell.self)", bundle: nil),
                               forCellReuseIdentifier: reuseIdentifier)
        } else {
            tableView.register(Cell.self, forCellReuseIdentifier: reuseIdentifier)
        }
    }
}
