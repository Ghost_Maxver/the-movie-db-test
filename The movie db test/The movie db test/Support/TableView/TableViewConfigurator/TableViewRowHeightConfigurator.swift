//
//  TableViewRowHeightConfigurator.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

protocol TableViewRowHeightConfigurable {
    associatedtype Item
    func tableView(_ tableView: UITableView, heightForRowWithItem item: Item) -> CGFloat
}

struct TableViewRowHeightConfigurator<Item>: TableViewRowHeightConfigurable {
    typealias RowConfigurator = (UITableView, Item) -> CGFloat
    
    let configurator: RowConfigurator
    
    func tableView(_ tableView: UITableView, heightForRowWithItem item: Item) -> CGFloat {
        return configurator(tableView, item)
    }
}
