//
//  UIViewController+Extensions.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

extension UIViewController {
    static func topViewController(
        controller: UIViewController? =
        UIApplication.shared.windows.filter { $0.isKeyWindow }.first?.rootViewController) -> UIViewController? {

        if let navigationController: UINavigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }

        if let tabController: UITabBarController = controller as? UITabBarController {
            if let selected: UIViewController = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }

        if let presented: UIViewController = controller?.presentedViewController {
            return topViewController(controller: presented)
        }

        return controller
    }
    
    static func dismiss(viewController: UIViewController, completion: EmptyCallback?) {
        if let presentedViewController = viewController.presentedViewController {
            self.dismiss(viewController: presentedViewController, completion: {
                viewController.dismiss(animated: true, completion: {
                    completion?()
                })
            })
        } else {
            completion?()
        }
    }
}
