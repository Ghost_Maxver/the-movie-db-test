//
//  NavigationController.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    enum NavigationDesign {
        static let tintColor = UIColor(named: "navigation_tint")
        static let barTintColor = UIColor(named: "navigation_background")
    }
}
