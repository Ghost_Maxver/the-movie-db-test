//
//  AppDelegatesFactory.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

enum AppDelegatesFactory {
    static func makeDefault() -> AppDelegateType {
        let launchAppDelegate = LaunchAppDelegate()
        let networkReachabilityAppDelegate = NetworkReachabilityAppDelegate()

        return CompositeAppDelegate(appDelegates: [
            launchAppDelegate,
            networkReachabilityAppDelegate
        ])
    }
}
