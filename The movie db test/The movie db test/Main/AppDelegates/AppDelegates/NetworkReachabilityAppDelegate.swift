//
//  NetworkReachabilityAppDelegate.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit
import Reachability

class NetworkReachabilityAppDelegate: AppDelegateType {
    
    private var reachability: Reachability?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        configureReachability()
        subscribeOnNotification()
        return true
    }
    
    // MARK: - Private API
    
    private func configureReachability() {
        self.reachability = try? Reachability()
        
        reachability?.whenUnreachable = { [weak self] _ in
            guard let self = self else { return }

            if self.isWindowAvailable() {
                NotificationBannerFactory.makeDefaultInfoBanner(title: "Check your internet connection",
                                                                duration: 1).show()
            }
        }

        try? reachability?.startNotifier()
    }
    
    private func subscribeOnNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didBecomeActive),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }

    @objc
    private func didBecomeActive() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if self.reachability?.connection == Reachability.Connection.unavailable {
                if self.isWindowAvailable() {
                    NotificationBannerFactory.makeDefaultInfoBanner(title: "Check your internet connection",
                                                                    duration: 1).show()
                }
            }
        }
    }
    
    private func isWindowAvailable() -> Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.connectedScenes
                .first { $0.activationState == .foregroundActive }
                .map { $0 as? UIWindowScene }
                .map { $0?.windows.first } ?? UIApplication.shared.delegate?.window != nil
        }

        return UIApplication.shared.delegate?.window != nil
    }
}
