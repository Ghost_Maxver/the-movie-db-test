//
//  LanchAppDelegate.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

class LaunchAppDelegate: AppDelegateType {
    private let window: UIWindow
    private let rootRouter: RootRouter
    
    override init() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.rootRouter = DefaultRootRouter(window: window)
    }
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        rootRouter.openInitialScreen()
        return true
    }
}
