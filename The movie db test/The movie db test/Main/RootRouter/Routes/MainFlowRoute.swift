//
//  MainFlowRoute.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

enum MainFlowRoute {
    static func installInitialScreen(into window: UIWindow) {
        let tabBarController = MainTabBarAssembly.initializeModule()

        window.rootViewController = tabBarController

        if !window.isKeyWindow {
            window.makeKeyAndVisible()
        }
    }
}
