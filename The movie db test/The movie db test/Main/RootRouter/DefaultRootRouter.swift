//
//  DefaultRootRouter.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

class DefaultRootRouter: RootRouter {

    // MARK: - Private Properties
    
    private let window: UIWindow

    // MARK: - Initializers
    
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK: - Public API
    
    func openInitialScreen() {
        MainFlowRoute.installInitialScreen(into: self.window)
        UIView.transition(with: window, duration: 0.25, options: .transitionCrossDissolve, animations: nil, completion: nil)
    }
    
    // MARK: - Private API
}
