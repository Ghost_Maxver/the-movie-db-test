//
//  ApplicationAssembly.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation
import Storage

class AppAssembly: AppAssemblyType {

    // MARK: - Public Properties

    let configuration: AppConfigurationType
    let networkingManager: NetworkingManager
//    let storage: Storage

    // MARK: - Private Properties

    // MARK: - Initializers

    init() {
        self.configuration = DefaultAppConfiguration()
        self.networkingManager = DefaultNetworkingManager(requestManager: RequestManager(),
                                                          baseURL: configuration.apiBaseURL)
//        self.storage = CoreDataStorage.loadingStorage()
    }

    // MARK: - Public API

    // MARK: - Private API
}
