//
//  ApplicationAssemblyType.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation
//import Storage

protocol AppAssemblyType {
    var configuration: AppConfigurationType { get }
    var networkingManager: NetworkingManager { get }
//    var storage: Storage { get }
}
