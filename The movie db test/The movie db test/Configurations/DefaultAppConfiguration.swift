//
//  DefaultAppConfiguration.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

class DefaultAppConfiguration: AppConfigurationType {
    let apiKey: String
    let apiBaseURL: BaseURL
    
    init() {
        self.apiKey = Constants.apiKey
        self.apiBaseURL = BaseURL(scheme: Constants.networkURLScheme,
                                  host: Constants.networkURLHost)
    }
}

private extension DefaultAppConfiguration {
    enum Constants {
        static let apiKey = "d6d41423962e8bea1ef5617c08132f1f"
        static let networkURLScheme: BaseURL.URLScheme = .https
        static let networkURLHost = "api.themoviedb.org"
    }
}
