//
//  RequestErrorMapper.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya
import Alamofire

enum RequestErrorNotification {
    static var serverError: Notification.Name {
        return Notification.Name("request.server.error.notification")
    }
}

class RequestErrorMapper: RequestErrorMapping {
    func map(_ moyaError: MoyaError) -> RequestError {
        
        switch moyaError {
        case .imageMapping,
             .jsonMapping,
             .stringMapping,
             .objectMapping,
             .encodableMapping,
             .parameterEncoding,
             .requestMapping:
        return .objectMapping
        case .statusCode(let response):
            return mapStatusCode(response: response)
        case .underlying(let error as AFError, let response) where error.underlyingError != nil:
            return mapUnderlyingError(error.underlyingError!, response: response)
        case .underlying(let error, let response):
            return mapUnderlyingError(error, response: response)
        }
    }
    
    private func mapStatusCode(response: Moya.Response) -> RequestError {
        switch response.statusCode {
        case let redirectionMessage where (300..<400).contains(redirectionMessage):
            return .otherStatusCode(response: response)
        case let clientError where (400..<500).contains(clientError):
            return mapClientError(code: clientError, response: response)
        case let serverError where (500..<600).contains(serverError):
            fireServerError()
            return .server(response: response)
        default:
            return .otherStatusCode(response: response)
        }
    }
    
    private func fireServerError() {
        NotificationCenter.default.post(name: RequestErrorNotification.serverError, object: nil)
    }
    
    private func mapClientError(code: Int, response: Response) -> RequestError {
        switch code {
        case 401:
            return .unauthorized
        default:
            return .client(response: response)
        }
    }
    
    private func mapUnderlyingError(_ error: Error, response: Moya.Response?) -> RequestError {
        switch (error as NSError).code {
        case NSURLErrorTimedOut:
            return .timeout
        case NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost:
            return .noInternetConnection
        case NSURLErrorCannotConnectToHost:
            return .cannotConnectToHost
        default:
            return .unknown(error, response)
        }
    }
}
