//
//  RequestError.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya

public enum RequestError: Error {
    case noInternetConnection
    case cannotConnectToHost
    case timeout
    case unauthorized
    case client(response: Moya.Response)
    case server(response: Moya.Response)
    case otherStatusCode(response: Moya.Response)
    case objectMapping
    case unknown(Error, Moya.Response?)
}

extension RequestError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noInternetConnection, .timeout:
            return "Poor Internet connection"
        case .cannotConnectToHost:
            return "Unable to connect to host"
        case .unauthorized:
            return "Unauthorized user"
        case .client(let response):
            return "Client error. Code \(response.statusCode)"
        case .server(let response):
            return "Server error. Code \(response.statusCode)"
        case .otherStatusCode(let response):
            return "Another error. Code \(response.statusCode)"
        case .objectMapping:
            return "Parsing error"
        case .unknown:
            return "Oops! Something went wrong"
        }
    }
}
