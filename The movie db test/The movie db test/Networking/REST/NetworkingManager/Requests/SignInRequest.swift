//
//  SignInRequest.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya
import Combine

// MARK: - SignInWithPhoneNumberConfirmRequestExecutable

extension DefaultNetworkingManager: SignInWithPhoneNumberConfirmRequestExecutable {
    func singIn(phoneNumber: String, pin: String) -> AnyPublisher<Void, RequestError> {
        let target = makeEndpointBuilder()
            .method(.post)
            .path("/auth/signIn")
            .task(makeBody(phoneNumber: phoneNumber, pin: pin))
            .build()
        
        return Future { [unowned self] (promise) in
            self.requestManager.execute(target,
                                        then: promise)
        }.map { _ in
            return Void()
        }.mapError {
            RequestErrorMapper().map($0)
        }.eraseToAnyPublisher()
    }
    
    private func makeBody(phoneNumber: String, pin: String) -> Moya.Task {
        let params = [BodyKey.telephone: phoneNumber,
                      BodyKey.pin: pin]
        
        return .requestParameters(parameters: params,
                                  encoding: JSONEncoding.default)
    }
}

// MARK: -

extension DefaultNetworkingManager {
    private enum BodyKey {
        static var telephone: String { "telephone" }
        static var pin: String { "pin" }
    }
}
