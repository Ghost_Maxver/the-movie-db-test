//
//  DefaultNetworkingManager.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

class DefaultNetworkingManager {
    let requestManager: RequestManagerType
    let baseURL: BaseURL
    
    init(requestManager: RequestManagerType, baseURL: BaseURL) {
        self.requestManager = requestManager
        self.baseURL = baseURL
    }
    
    func makeEndpointBuilder() -> EndPointBuilder {
        return EndPointBuilder(baseURL: baseURL)
    }
}
