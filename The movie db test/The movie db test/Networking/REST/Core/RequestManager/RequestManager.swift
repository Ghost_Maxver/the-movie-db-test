//
//  RequestManager.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya

class RequestManager: RequestManagerType {
    private let provider: MoyaProvider<Target>
    private let authenticationPlugin = AuthenticationPlugin()

    init() {
        #if DEBUG
        self.provider = MoyaProvider<Target>.init(plugins: [
            authenticationPlugin,
            NetworkLoggerPlugin(
                configuration: NetworkLoggerPlugin.Configuration(logOptions: [
                    .formatRequestAscURL,
                    .errorResponseBody
                ]))
        ])
        #else
        self.provider = MoyaProvider<Target>.init(plugins: [authenticationPlugin])
        #endif
    }

    func execute<DataMapper, ErrorMapper>(
        _ target: Target,
        mapUsing mapper: DataMapper,
        mapErrorUsing errorMapper: ErrorMapper,
        then completion: @escaping (_ result: Result<DataMapper.OutputModel, ErrorMapper.Output>) -> Void
    ) where DataMapper: JSONDataDecodable, ErrorMapper: RequestErrorMapping {
        self.execute(target, mapUsing: mapper,
                     then: { result in
                        switch result {
                        case .success(let response):
                            completion(.success(response))
                        case .failure(let moyaError):
                            let mappedError = errorMapper.map(moyaError)
                            completion(.failure(mappedError))
                        }
        })
    }
    
    func execute<D: JSONDataDecodable>(_ target: Target,
                                       mapUsing mapper: D,
                                       then completion: @escaping (_ result: Result<D.OutputModel, MoyaError>) -> Void) {

        self.execute(target, callbackQueue: .global()) { (result) in

            do {
                // 1. Try to get response
                let response = try result.get()
                
                // 2. Check status code is in range of success codes, if not throw Moya.Error.statusCode
                try self.validateForStatusCode(response: response, target: target)
                
                // 3. Map JSON Data response with specified mapper to concrete model
                let mappedModel = try self.map(response, mapper: mapper).get()
                
                // 4. Everything is ok. Return mapped model
                completion(.success(mappedModel))
            } catch let error as MoyaError {
                // Something went wrong during step 1, 2 or 3
                completion(.failure(error))
            } catch let error {
                // Falling to this catch is treated as unexpected behavior
                completion(.failure(.underlying(error, nil)))
            }
        }
    }

    func execute(_ target: Target, then completion: @escaping Moya.Completion) {
        _ = self.execute(target, completionClosure: { (result) in
            do {
                // 1. Try to get response
                let response = try result.get()
                
                // 2. Check status code is in range of success codes, if not throw Moya.Error.statusCode
                try self.validateForStatusCode(response: response, target: target)
                                
                completion(.success(response))
            } catch let error as MoyaError {
                // Something went wrong during step 1 or 2
                completion(.failure(error))
            } catch let error {
                // Falling to this catch is treated as unexpected behavior
                completion(.failure(.underlying(error, nil)))
            }
        })
    }

    // MARK: - Private API
    
    @discardableResult
    private func execute(_ target: Target,
                         callbackQueue: DispatchQueue = DispatchQueue.main,
                         progressClosure: Moya.ProgressBlock? = nil,
                         completionClosure: @escaping Moya.Completion) -> Cancellable {
        return provider.request(target, callbackQueue: callbackQueue, progress: progressClosure, completion: completionClosure)
    }

    private func map<D: JSONDataDecodable>(_ moyaResponse: Moya.Response, mapper: D) -> Result<D.OutputModel, MoyaError> {

        do {
            let mappedModel = try mapper.decodeJSONData(moyaResponse.data)
            return .success(mappedModel)
        } catch let error {
            let moyaError = MoyaError.objectMapping(error, moyaResponse)
            return .failure(moyaError)
        }
    }
    
    private func validateForStatusCode(response: Moya.Response, target: Target) throws {
        _ = try response.filterSuccessfulStatusCodes()
    }
}
