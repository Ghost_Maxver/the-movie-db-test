//
//  AuthenticationPlugin.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya

enum AuthMethodType {
    case none
    case bearer(token: String)
    
    var prefix: String? {
        switch self {
        case .none: return nil
        case .bearer: return "Bearer"
        }
    }
}

protocol AuthMethodTypeProvidable {
    var authorizationType: AuthMethodType { get }
}

class AuthenticationPlugin: PluginType {
    
    // MARK: - Methods(PluginType)
    
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        guard
            let target = target as? (TargetType & AuthMethodTypeProvidable)
        else {
            return request
        }
        
        switch target.authorizationType {
        case .none:
            return request
        case .bearer(let token):
            return addAuthHeaderToRequest(request, prefix: target.authorizationType.prefix, token: token)
        }
    }
    
    private func addAuthHeaderToRequest(_ request: URLRequest, prefix: String?, token: String) -> URLRequest {
        var request = request
        let value: String
        if let prefix = prefix {
            value = "\(prefix) \(token)"
        } else {
            value = token
        }
        request.addValue(value, forHTTPHeaderField: "token")
        
        return request
    }
}
