//
//  RequestManagerType.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya

protocol RequestManagerType {
    typealias Target = TargetEndPoint
    
    func execute<DataMapper, ErrorMapper>(
        _ target: Target,
        mapUsing mapper: DataMapper,
        mapErrorUsing errorMapper: ErrorMapper,
        then completion: @escaping (_ result: Result<DataMapper.OutputModel, ErrorMapper.Output>) -> Void
    ) where DataMapper: JSONDataDecodable, ErrorMapper: RequestErrorMapping
    
    func execute<D: JSONDataDecodable>(_ target: Target,
                                       mapUsing mapper: D,
                                       then completion: @escaping (_ result: Result<D.OutputModel, MoyaError>) -> Void)
    
    func execute(_ target: Target, then completion: @escaping Moya.Completion)
}
