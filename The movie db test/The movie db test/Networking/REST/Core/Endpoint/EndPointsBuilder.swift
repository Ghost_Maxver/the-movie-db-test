//
//  EndPointsBuilder.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya

class EndPointBuilder {
    typealias Headers = [String: String]
    
    private let baseURL: BaseURL
    private var path: String = ""
    private var method: Moya.Method = .get
    private var sampleData = Data()
    private var task: Moya.Task = .requestPlain
    private var headers: Headers?
    private var authorizationType: AuthMethodType = .none
    
    init(baseURL: BaseURL) {
        self.baseURL = baseURL
    }
    
    /// The path to be appended to `baseURL` to form the full `URL`.
    @discardableResult
    func path(_ path: String) -> Self {
        self.path = path
        return self
    }
    
    /// The HTTP method used in the request.
    @discardableResult
    func method(_ method: Moya.Method) -> Self {
        self.method = method
        return self
    }
    
    /// The type of HTTP task to be performed.
    @discardableResult
    func sampleData(_ sampleData: Data) -> Self {
        self.sampleData = sampleData
        return self
    }
    
    /// The type of HTTP task to be performed.
    @discardableResult
    func task(_ task: Moya.Task) -> Self {
        self.task = task
        return self
    }
        
    /// The headers to be used in the request.
    /// Overrides all existing headers.
    @discardableResult
    func headers(_ headers: Headers?) -> Self {
        self.headers = headers
        return self
    }
    
    /// Add the headers to be used in the request.
    /// Overrides existing key-value if it matches the new one.
    @discardableResult
    func addHeaders(_ headers: Headers) -> Self {
        if self.headers == nil {
            self.headers = [:]
        }
        
        self.headers?.merge(headers) { (_, new) in new }
        return self
    }
    
    /// Authorization method to be used in the request
    @discardableResult
    func authorizationType(_ authMethodType: AuthMethodType) -> Self {
        self.authorizationType = authMethodType
        return self
    }
    
    func build() -> TargetEndPoint {
        return TargetEndPoint(baseURL: self.baseURL.asURL,
                              path: self.path,
                              method: self.method,
                              sampleData: self.sampleData,
                              task: self.task,
                              headers: self.headers,
                              authorizationType: self.authorizationType)
    }
}
