//
//  BaseURL.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

struct BaseURL {
    enum URLScheme: String {
        case http
        case https
        case webSocket = "ws"
        case webSocketSecure = "wss"
    }
    
    let scheme: URLScheme
    let host: String
    
    init(scheme: URLScheme, host: String) {
        self.scheme = scheme
        self.host = host
    }
    
    var asURL: URL! {
        return URL(string: self.asString)
    }
    
    var asString: String {
        return "\(scheme.rawValue)://\(host)"
    }
}
