//
//  TargetEndPoint.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya

struct TargetEndPoint: TargetType, AuthMethodTypeProvidable {
    /// The target's base `URL`.
    var baseURL: URL

    /// The path to be appended to `baseURL` to form the full `URL`.
    var path: String

    /// The HTTP method used in the request.
    var method: Moya.Method

    /// Provides stub data for use in testing.
    var sampleData: Data

    /// The type of HTTP task to be performed.
    var task: Task

    /// The headers to be used in the request.
    var headers: [String: String]?
        
    /// Authrozation method to be used in the request
    var authorizationType: AuthMethodType = .none
}
