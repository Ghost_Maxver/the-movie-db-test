//
//  SimpleJSONDataMapper.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

class SimpleJSONDataMapper<D: Decodable>: JSONDataDecodable {
    
    func decodeJSONData(_ data: Data) throws -> D {
        return try JSONDecoder().decode(D.self, from: data)
    }
}
