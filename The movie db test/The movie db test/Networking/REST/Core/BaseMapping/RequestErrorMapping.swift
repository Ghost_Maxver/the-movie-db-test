//
//  RequestErrorMapping.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Moya

protocol RequestErrorMapping {
    associatedtype Output
    func map(_ moyaError: MoyaError) -> Output
}
