//
//  UserAccessToken.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

struct UserAccessToken: Codable, ExpressibleByStringLiteral, Equatable {
    private var accessToken: String
    
    init(stringLiteral value: String) {
        self.accessToken = value
    }
    
    var stringValue: String {
        return accessToken
    }
}
