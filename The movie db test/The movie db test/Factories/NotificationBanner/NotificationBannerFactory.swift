//
//  NotificationBannerFactory.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import NotificationBannerSwift

enum NotificationBannerFactory {
    static func makeDefaultSuccessBanner(title: String? = nil,
                                         subtitle: String? = nil,
                                         duration: TimeInterval = 0.25) -> NotificationBanner {
        let banner = NotificationBanner(title: title, subtitle: subtitle, style: .success)
        banner.duration = duration
        return banner
    }
    
    static func makeDefaultInfoBanner(title: String? = nil,
                                      subtitle: String? = nil,
                                      duration: TimeInterval = 0.25) -> NotificationBanner {
        let banner = NotificationBanner(title: title, subtitle: subtitle, style: .info)
        banner.duration = duration
        return banner
    }
}
