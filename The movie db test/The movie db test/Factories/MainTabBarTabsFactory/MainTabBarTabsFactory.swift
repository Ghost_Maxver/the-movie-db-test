//
//  MainTabBarTabsFactory.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 26.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

enum MainTabBarTabsFactory {

    static var makeFilms: UIViewController {
        let filmsVC = FilmsAssembly.initializeModule().viewController

//        filmsVC.tabBarItem = UITabBarItem(title: Localized.profile(),
//                                            image: R.image.profileTabIcon(),
//                                            selectedImage: R.image.profileTabIcon())

        let navigationController = NavigationControllerFactory.makeNavigationController()
        navigationController.setViewControllers([filmsVC], animated: false)
        navigationController.navigationBar.prefersLargeTitles = true

        return navigationController
    }
    
    static var makeFavoriteFilms: UIViewController {
        return UIViewController()
    }
}
