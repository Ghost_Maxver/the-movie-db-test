//
//  NavigationControllerFactory.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

enum NavigationControllerFactory {
    static func makeNavigationController() -> UINavigationController {
        let navigationController = NavigationController()
        navigationController.navigationBar.tintColor = NavigationController.NavigationDesign.tintColor
        navigationController.navigationBar.barTintColor = NavigationController.NavigationDesign.barTintColor
        return navigationController
    }
}
