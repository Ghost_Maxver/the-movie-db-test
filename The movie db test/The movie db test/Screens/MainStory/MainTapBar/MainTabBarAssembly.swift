//
//  MainTabBarAssembly.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

enum MainTabBarAssembly {
    static func initializeModule() -> UIViewController {
        let view: MainTabBarViewController = UIStoryboard.loadViewController()
        let presenter = MainTabBarPresenter()

        view.presenter = presenter
        presenter.view = view
        
        return view
    }
}
