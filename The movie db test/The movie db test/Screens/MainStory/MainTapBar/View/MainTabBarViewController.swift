//
//  MainTabBarViewController.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController, MainTabBarViewType, UITabBarControllerDelegate {
    var presenter: MainTabBarPresenterType!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.viewDidSetup()
    }
    
    // MARK: - Public API
    
    func embedTabs() {
        viewControllers = [MainTabBarTabsFactory.makeFilms,
                           MainTabBarTabsFactory.makeFavoriteFilms]
        selectedIndex = 0
    }
}
