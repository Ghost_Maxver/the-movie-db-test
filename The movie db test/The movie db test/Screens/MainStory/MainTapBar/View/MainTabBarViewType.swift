//
//  MainTabBarViewType.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

protocol MainTabBarViewType: class {
    func embedTabs()
}
