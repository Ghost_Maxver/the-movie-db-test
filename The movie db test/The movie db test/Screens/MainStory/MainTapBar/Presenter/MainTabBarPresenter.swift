//
//  MainTabBarPresenter.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

class MainTabBarPresenter: MainTabBarPresenterType {
    weak var view: MainTabBarViewType!
    
    func viewDidSetup() {
        view.embedTabs()
    }
}
