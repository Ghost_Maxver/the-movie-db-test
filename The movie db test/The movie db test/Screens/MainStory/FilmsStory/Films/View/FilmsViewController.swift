//
//  FilmsViewController.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 26.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

class FilmsViewController: BaseViewController, FilmsViewType {

    // MARK: - Public properties

    var presenter: FilmsPresenterType!
    
    // MARK: - Outlets
    
    // MARK: - Private properties
   
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    // MARK: - Private API

    private func configureUI() {
        
    }
    
    // MARK: - <FilmsViewType>
    
    func configureTableView() {
    }
}
