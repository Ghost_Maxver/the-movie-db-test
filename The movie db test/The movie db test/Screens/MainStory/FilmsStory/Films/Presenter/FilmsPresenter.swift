//
//  FilmsPresenter.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 26.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

class FilmsPresenter: BasePresenter, FilmsPresenterType {
    
    // MARK: - Public Properties
    
    weak var view: FilmsViewType!
    
    // MARK: - Private Properties
    
    // MARK: - FilmsPresenterType
    
    override func viewDidLoad() {
        
    }

    override func viewDidAppear() {
        
    }
}
