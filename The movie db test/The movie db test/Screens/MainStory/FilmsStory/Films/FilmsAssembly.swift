//
//  FilmsAssembly.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 26.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

enum FilmsAssembly {
    static func initializeModule() -> ViewType {
        let view: FilmsViewController = UIStoryboard.loadViewController()
        let presenter = FilmsPresenter()
        
//        let chatsListTableAdapter = ChatsListTableAdapterFactory.makeTableViewAdapter
//        chatsListTableAdapter.delegate = presenter
        
        view.presenter = presenter
        presenter.view = view
        
        return view
    }
}
