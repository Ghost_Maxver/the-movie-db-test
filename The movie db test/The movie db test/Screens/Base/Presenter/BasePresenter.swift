//
//  BasePresenter.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 26.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

open class BasePresenter: PresenterType {
    
    // MARK: - PresenterType
    
    func viewDidLoad() {
        
    }
    
    func firstViewWillAppear() {
        
    }
    
    func viewWillAppear() {
        
    }
    
    func viewDidAppear() {
        
    }
    
    func viewWillDisappear() {
        
    }
    
    func viewDidDisappear() {
        
    }
}
