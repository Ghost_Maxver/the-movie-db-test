//
//  ViewType.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

protocol ViewType: AnyObject {
    var viewController: UIViewController { get }
    func close()
    func showActivity()
    func hideActivity()
    func showAlertWithError(_ error: Error)
}

extension ViewType where Self: UIViewController {
    var viewController: UIViewController {
        return self
    }
}
