//
//  ActivityAdapterType.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 03.12.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

protocol ActivityAdapterType {
    func configureWith(view: UIView)
    func show()
    func hide()
}
