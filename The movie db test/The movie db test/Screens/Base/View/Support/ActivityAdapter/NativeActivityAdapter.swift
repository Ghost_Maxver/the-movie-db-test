//
//  NativeActivityAdapter.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 03.12.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

class NativeActivityAdapter: ActivityAdapterType {
    
    // MARK: - Private properties

    private weak var parentView: UIView?
    private lazy var activity: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView(style: .medium)
        activity.hidesWhenStopped = true
        return activity
    }()
    private lazy var backgroundView = UIView()
    
    // MARK: - Public API

    func configureWith(view: UIView) {
        parentView = view
        
        backgroundView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundView.backgroundColor = Constants.backgroundViewColor
        
        activity.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleLeftMargin, .flexibleRightMargin]
    }
    
    func show() {
        guard let parentView: UIView = parentView else { return }
        
        hide()
        
        backgroundView.frame = parentView.bounds
        parentView.addSubview(backgroundView)
        
        activity.center = CGPoint(x: backgroundView.bounds.width/2.0, y: backgroundView.bounds.height/2.0)
        backgroundView.addSubview(activity)
        backgroundView.superview?.bringSubviewToFront(backgroundView)
        activity.startAnimating()
    }
    
    func hide() {
        activity.stopAnimating()
        backgroundView.removeFromSuperview()
        activity.removeFromSuperview()
    }
}

private extension NativeActivityAdapter {
    enum Constants {
        static let backgroundViewColor: UIColor = UIColor.black.withAlphaComponent(0.03)
    }
}
