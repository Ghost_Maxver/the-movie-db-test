//
//  BaseViewController.swift
//  The movie db test
//
//  Created by Maksym Vereshchaka on 26.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Public properties
    
    var basePresenter: PresenterType?
    var callBack: Callback<Any?>?
    
    var isModal: Bool {
        return navigationController?.children.first == self || navigationController == nil
    }
    
    lazy var activity: ActivityAdapterType = NativeActivityAdapter()
    
    // MARK: - Private properties
    
    private var isFirstAppear = true
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basePresenter = createPresenter()
        basePresenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFirstAppear {
            basePresenter?.firstViewWillAppear()
        }

        basePresenter?.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        isFirstAppear = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
        
        basePresenter?.viewWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        basePresenter?.viewDidDisappear()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Public API
    
    func performCallBackWith(sender: Any?, goBack isGoBack: Bool) {
        callBack?(sender)
        
        if isGoBack {
            close()
        }
    }
    
    func showActivity() {
        activity.show()
    }

    func hideActivity() {
        activity.hide()
    }
    
    func close() {
        if isModal {
            dismiss(animated: true, completion: nil)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func showAlertWithError(_ error: Error) {
        showAlertController(title: "Error", message: error.localizedDescription)
    }
    
    // MARK: - For override
    
    func createPresenter() -> BasePresenter? {
        
        return nil
    }
}
