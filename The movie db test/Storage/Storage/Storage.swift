//
//  Storage.swift
//  Storage
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import CoreStore

public typealias Storage = StorageLoadable & StorageCleanable & FilmsStorable

// MARK: - StorageLoadable

public enum StorageState {
    case unknown
    case loading
    case failedToLoad(Error)
    case loaded
}

public protocol StorageLoadable: AnyObject {
    var storageState: StorageState { get }

    func loadStorage(on dispatchQueue: DispatchQueue, completion: @escaping (Result<Void, Error>) -> Void)
    func loadStorage(completion: @escaping (Result<Void, Error>) -> Void)
}

public protocol StorageCleanable: AnyObject {
    func cleanStorage()
}

// MARK: - FilmsStorable

public protocol FilmsStorable: AnyObject {
    
}
