//
//  CoreDataStorageConfigs.swift
//  Storage
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

public struct CoreDataStorageConfigs {
    let storageLocation: StorageLocation
    
    init(storageLocation: StorageLocation = .standard) {
        self.storageLocation = storageLocation
    }
    
    enum StorageLocation {
        case standard
        case defaultPath(fileName: String)
        case defaultFileName(directoryPath: URL)
        case fileURL(URL)
    }
}
