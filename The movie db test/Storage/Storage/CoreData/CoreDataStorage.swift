//
//  CoreDataStorage.swift
//  Storage
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import CoreStore

public final class CoreDataStorage: StorageLoadable {
    let dataStack: DataStack
    var storage: LocalStorage!
    let storageFileURL: URL
    
    public let configs: CoreDataStorageConfigs
    
    public private(set) var storageState: StorageState = .unknown
    
    // MARK: - Initializers
    
    init(configs: CoreDataStorageConfigs = .init()) {
        self.configs = configs
        self.dataStack = Self.makeDataStack()
        self.storageFileURL = Self.storageFileURL(for: configs)
    }
    
    // MARK: - Public API
    
    /**
       Immediately return without blocking and load storage on background queue
    */
    public static func loadingStorage() -> CoreDataStorage {
        let storage = CoreDataStorage()
        storage.loadStorage(completion: {_ in})
        return storage
    }
    
    public func loadStorage(completion: @escaping (Result<Void, Error>) -> Void) {
        self.loadStorage(on: .global(), completion: completion)
    }
    
    public func loadStorage(on dispatchQueue: DispatchQueue, completion: @escaping (Result<Void, Error>) -> Void) {
        storageState = .loading
        
        dispatchQueue.async { [weak self] in
            guard let self = self else {
                assertionFailure()
                return
            }
            
            do {
                self.storage = try Self.createStorage(byAddingTo: self.dataStack, fileURL: self.storageFileURL)
                self.storageState = .loaded
                completion(.success(Void()))
            } catch let error {
                self.storageState = .failedToLoad(error)
                completion(.failure(error))
            }
        }
    }
    
    // MARK: - Private API
    
    static func storageFileURL(for config: CoreDataStorageConfigs) -> URL {
        switch config.storageLocation {
        case .standard:
            return NSPersistentContainer.defaultDirectoryURL()
                .appendingPathComponent(Self.defaultDatabaseFileName)
                .appendingPathExtension(Self.sqliteExtension)
        case .defaultPath(fileName: let fileName):
            return NSPersistentContainer.defaultDirectoryURL()
                .appendingPathComponent(fileName)
                .appendingPathExtension(Self.sqliteExtension)
        case .defaultFileName(directoryPath: let directoryPath):
            return directoryPath
                .appendingPathComponent(Self.defaultDatabaseFileName)
                .appendingPathExtension(Self.sqliteExtension)
        case .fileURL(let path):
            return path
        }
    }
    
    /**
       Synchronously  initialize the storage.
    */
    private static func createStorage(byAddingTo dataStack: DataStack, fileURL: URL) throws -> LocalStorage {
        return try dataStack.addStorageAndWait(SQLiteStore(fileURL: fileURL, localStorageOptions: .allowSynchronousLightweightMigration))
    }
    
    private static func makeDataStack() -> DataStack {
        return DataStack(xcodeModelName: Self.momFileName,
                         bundle: Bundle(for: self))
    }
}

// MARK: - StorageCleanable

extension CoreDataStorage: StorageCleanable {
    public func cleanStorage() {
        dataStack.perform(
            asynchronous: { transaction -> Void in
        }, completion: { _ in })
    }
}

// MARK: - Constants

extension CoreDataStorage {
    static var defaultDataBaseFileDirectoryURL: URL {
        return NSPersistentContainer.defaultDirectoryURL()
    }
    
    private static var defaultDatabaseFileName: String {
        return "The movie db test"
    }
    
    private static var sqliteExtension: String {
        return ".sqlite"
    }
    
    private static var momFileName: String {
        return "Model"
    }
}
