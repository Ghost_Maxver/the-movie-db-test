//
//  Result+Extension.swift
//  Storage
//
//  Created by Maksym Vereshchaka on 25.11.2020.
//  Copyright © 2020 Test app. All rights reserved.
//

import Foundation

extension Result {
    var eraseError: Result<Success, Error> {
        return self.mapError({ $0 as Error })
    }
}
